package net.albertoi.app.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import net.albertoi.library.domain.Hello;
import net.albertoi.library.service.api.HelloService;


@RestController
public class HelloController {

  private final HelloService service;

  @Autowired
  public HelloController(HelloService service) {
    this.service = service;
  }

  @GetMapping("/hello")
  public Hello requestHello(@RequestParam(name = "message", required = false) String message) {
    if (message != null) {
      return service.message(message);
    }
    return service.defaultMessage();
  }

  @GetMapping("/exception")
  public void requestException() {
    service.throwException();
  }

  @GetMapping("/library/exception")
  public void requestLibraryException() {
    service.throwLibraryException();
  }
}
