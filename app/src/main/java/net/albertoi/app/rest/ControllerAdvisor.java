package net.albertoi.app.rest;


import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import net.albertoi.app.domain.ErrorDTO;

@Slf4j
@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {


  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity<Object> handleCityNotFoundException(Exception ex) {
    ErrorDTO error = defaultError(ex);

    if (StringUtils.startsWith(ex.getMessage(), "LIBRARY")) {
      error = extractLibraryError(ex);
    }

    return new ResponseEntity<>(error, error.getHttpStatus());
  }


  private ErrorDTO defaultError(Exception ex) {
    return ErrorDTO.builder()
        .message(ex.getMessage())
        .code("GEN_001")
        .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        .build();
  }

  private ErrorDTO extractLibraryError(Exception ex) {
    String message = ex.getCause().getMessage();
    String code = ex.getCause().getCause().getMessage();
    HttpStatus status = HttpStatus
        .resolve(Integer.parseInt(ex.getCause().getCause().getCause().getMessage()));

    log.info("Message {}, code {}, status {}", message, code, status);

    return ErrorDTO.builder().message(message).code(code).httpStatus(status).build();
  }
}
