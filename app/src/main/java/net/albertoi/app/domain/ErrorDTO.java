package net.albertoi.app.domain;

import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder()
public class ErrorDTO {

  private final String message;
  private final String code;
  private final HttpStatus httpStatus;
  private final LocalDateTime timestamp = LocalDateTime.now();

}
