package net.albertoi.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication
public class Library {

	public static void main(String[] args) {
		SpringApplication.run(Library.class, args);
	}

}
