package net.albertoi.library.service.api;

import net.albertoi.library.domain.Hello;

public interface HelloService {

  Hello defaultMessage();

  Hello message(String message);

  void throwException() throws RuntimeException;

  void throwLibraryException() throws RuntimeException;
}
