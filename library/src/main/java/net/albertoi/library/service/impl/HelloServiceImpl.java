package net.albertoi.library.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.albertoi.library.config.HelloProperties;
import net.albertoi.library.domain.Hello;
import net.albertoi.library.exception.HelloException;
import net.albertoi.library.service.api.HelloService;

@Service
@Slf4j
@RequiredArgsConstructor
public class HelloServiceImpl implements HelloService {

  private final HelloProperties properties;

  @Override
  public Hello defaultMessage() {
    log.info("Getting default message");
    return Hello.builder().message(properties.getDefaultMessage()).build();
  }

  @Override
  public Hello message(String message) {
    log.info("Getting message");
    return Hello.builder().message(message).build();
  }

  @Override
  public void throwLibraryException() {
    throw new HelloException("Error", "ERR_001", HttpStatus.BAD_GATEWAY);
  }

  @Override
  public void throwException() {
    throw new HelloException();
  }
}
