package net.albertoi.library.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Hello {
  private final String message;
}
