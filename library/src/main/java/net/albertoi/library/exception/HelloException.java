package net.albertoi.library.exception;

import org.springframework.http.HttpStatus;

public class HelloException extends RuntimeException {

  private final static String LIBRARY = "LIBRARY";

  public HelloException(String message, String code, HttpStatus httpStatus) {
    super(LIBRARY,
        new Throwable(message,
            new Throwable(code,
                new Throwable(String.valueOf(httpStatus.value())))));
  }

  public HelloException() {
    super();
  }
}
